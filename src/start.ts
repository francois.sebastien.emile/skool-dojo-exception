import { applicationConfiguration } from "./config";

import { Application } from "./application/Application";
import { listAccountsRoute } from "./application/endpoints/AccountEndpoint";

Application.run(applicationConfiguration, listAccountsRoute);