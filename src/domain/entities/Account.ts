export class Account {

  constructor(private _numero: string, private _solde: number) { }

  public get numero(): string {
    return this._numero;
  }

  public get solde(): number {
    return this._solde;
  }

}