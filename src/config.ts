import { ServerConfiguration } from "./application/ServerConfiguration";

export const applicationConfiguration: ServerConfiguration = {
  appHost: String(process.env.APP_HOST),
  appPort: Number(process.env.APP_PORT)
}