export interface ServerConfiguration {
  appHost: string;
  appPort: number;
}