import { ServerRoute } from "@hapi/hapi";

import { Container } from "../Container";

export const listAccountsRoute: ServerRoute = {
  method: "get",
  path: "/account/list",
  handler: Container.listAccountController.execute.bind(Container.listAccountController)
}