import { ListAccounts } from "../domain/usecases/ListAccounts";
import { ListAccountsController } from "./controllers/ListAccountsController";

export namespace Container {

  export const listAccount = new ListAccounts();
  export const listAccountController = new ListAccountsController(listAccount);

}