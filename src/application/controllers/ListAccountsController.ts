import { Request, ResponseToolkit } from "@hapi/hapi";

import { Account } from "../../domain/entities/Account";
import { ListAccounts } from "../../domain/usecases/ListAccounts";

export class ListAccountsController {

  constructor(private readonly listAccount: ListAccounts) { }

  public async execute(request: Request, response: ResponseToolkit): Promise<Account[]> {
    return this.listAccount.execute();
  }

}