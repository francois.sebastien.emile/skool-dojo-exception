import { Server, ServerRoute } from "@hapi/hapi";

import { ServerConfiguration } from "./ServerConfiguration";

export class Application {

  private readonly httpServer!: Server;

  private constructor(serverConfiguration: ServerConfiguration, routes?: ServerRoute | ServerRoute[]) {
    this.httpServer = new Server({
      host: serverConfiguration.appHost,
      port: serverConfiguration.appPort
    });

    if (routes) {
      this.httpServer.route(routes);
    }
  }

  public static async run(serverConfiguration: ServerConfiguration, ...routes: ServerRoute[]): Promise<void> {
    const application = new Application(serverConfiguration, routes);

    await application.start();
  }

  private async start(): Promise<void> {
    await this.httpServer.start();
    console.log(`[INFO]  Application has started at ${this.httpServer.info.host}:${this.httpServer.info.port}`);
  }

}